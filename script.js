// console.log("******* JS ARRAYS *******");
// /*

// Arrays - used to store multiple related values in a single variable.
// -declared using square brackets [] know as array literals

// Elements - are values inside an array.

// index - 0 = offset

// element 1 = index 0

// Syntax: let/const arrayName = [elementA, elementB, elementC]

// */

// let grades = [98, 94, 89, 90];
// let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// let mixedArr = [12, 'Asus', null, undefined, {}]; //not recommended
// console.log(mixedArr);


// //Reading
// console.log(grades[0]);
// console.log(computerBrands[3]);
// console.log(grades[10]); //this will result as undefined

// let myTasks = [

// 	'bake sass',
// 	'drink html',
// 	'inhale css',
// 	'eat javascript'	
// ]

// console.log(myTasks)

// myTasks[0] = 'hello world';
// console.log(myTasks);

// //Getting the length of an array
// console.log(computerBrands.length);

// let lastIndex = computerBrands.length - 1;
// console.log(lastIndex);

// /* 
// ARRAY METHODS

// 1. Mutator Methods
// - are functions that 'mutate' or change an array

// */

// //push 
// // - adds an element in the end of the array and return its lenght
// //syntax: arrayName.push(elementA, elementB)

// let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
// console.log('Current Array');
// console.log(fruits);
// console.log('Array after push()');

// let fruitsLength = fruits.push('Mango');
// console.log(fruits);
// console.log(fruitsLength);

// let fruitsLength2 = fruits.push('Banana');
// console.log(fruits);
// console.log(fruitsLength2);

// fruits.push('Guava', 'Avocado');
// console.log('Mutated array after push()');
// console.log(fruits);

// //pop - removes the last element in an array and return the removed element

// let removedFruit = fruits.pop();
// console.log(fruits);
// console.log(removedFruit);

// //shift - removes an element at the beginning of an array and return the remove element

// let firstFruit = fruits.shift();
// console.log('Mutated array after shift()');
// console.log(fruits);
// console.log(firstFruit);

// //unshift - adds one or more elements at the begnning of an array

// fruits.unshift('Lime', 'Papaya');
// console.log('Mutated array after unshift');
// console.log(fruits);

// //splice removes element from specified index and add new elements
// //syntax; arrayName.splice(startingIndex, deleteCount, elementsTobeAdded)

// // fruits.splice(1); // sample 1
// // fruits.splice(2, 2); // sample 2
// fruits.splice(1, 2, 'Cherry', 'Grapes');
// console.log('Mutated array after splice()');
// console.log	(fruits);

// //sort - rearranges the elements in alphanumeric order
// fruits.sort();
// console.log(fruits);

// //reverse - reverse the arrange of elements
// fruits.reverse();
// console.log(fruits);


// /*
// 2. Non Mutator Methods
// - are functions that do not modify or change the array
// */

// let countries =['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR','DE'];

// //indexOf()
// // - returns the first index of the first matching element found in an array
// //if no match is found, it returns -1

// let firstIndex = countries.indexOf('PH');
// console.log('Result of indexOf() method: ' + firstIndex);

// let invalidCountry = countries.indexOf('BR')
// console.log('Result of indexOf() method: ' + invalidCountry);

// //lastIndexOf()
// //returns the last matching element found in an array
// //syntax: arrayName.lastIndexOf(searchValue);
// //arrayName.lastIndexOf(searchValue, fromIndex);

// let lastIndexBeg = countries.lastIndexOf('PH');
// console.log('Result of indexOf() method: ' + lastIndexBeg);

// let lastIndexStart = countries.lastIndexOf('PH', 2);
// console.log('Result of indexOf() method: ' + lastIndexStart);

// //slice - slices a portion of an array and return a new array 

// //syntax arrayName.slice(startingIndex);
// //syntax arrayName.slice(startingIndex, endingIndex);

// let slicedArrayA = countries.slice(2);
// console.log('Result from slice method A');
// console.log(slicedArrayA);

// let slicedArrayB = countries.slice(2, 4);
// console.log('Result from slice method B');
// console.log(slicedArrayB);


// let slicedArrayC = countries.slice(-3);
// console.log('Result from slice method C');
// console.log(slicedArrayC);

// //toString

// let stringArray = countries.toString();
// console.log('Result from toString()');
// console.log(stringArray);

// //concat - combines two ore more arrays and rethrun the combined rseult

// let taskArrayA = ['drings HTML', 'eat javascript'];
// let taskArrayB = ['inhale CSS', 'breathe sass'];
// let taskArrayC = ['get git', 'be node'];

// let allTasks = taskArrayA.concat(taskArrayB, taskArrayC, 'smell express');
// console.log('Result from concat()');
// console.log(allTasks)


// // join - returns an array as string seperated by specified separators

// let users = ['John', 'Jane', 'Joe', 'Robert'];
// console.log(users.join());
// console.log(users.join(' '));
// console.log(users.join(' - '));



// /*
// 3. Iteration Methods
// - loops design to perform repetitive tasks on arrays

// */

// //foreach()
// /*syntax: arrayName.forEach(fucntion(individualElement)){
//  statement
// })
//  */

// //sample1

//  allTasks.forEach(function(task){

//  console.log(allTasks);

//  })

//  //sample 2 

//  let filteredTasks = [];

// allTasks.forEach(function(task){

// 		if(task.length > 10){

// 				filteredTasks.push(task)
// 		}
// })

// console.log('Result from forEach');
// console.log(filteredTasks);


// //map - iterates on each element and returns new array with different values depending on the result of the functions operation

// let numbers = [1,2,3,4,5];

// let numberMap = numbers.map(function(number){

// 	return number * number;
// })

// console.log('Result from the Map');
// console.log(numberMap)


// //sample2

// let numberMap2 = [];
// numbers.forEach(function(number){

// let square = number * number;
// numberMap2.push(square);

// });

// console.log('Result from forEach()');
// console.log(numberMap2)

// //string mapping

// let arrayMap = allTasks.map(function(task){

// 		return 'I ' + task;


// });
// console.log(arrayMap);

// //every

// let allValid = numbers.every(function(number){

// 		return (number < 3);

// })
// console.log('Result form every()');
// console.log(allValid);

// //some

// let someValid = numbers.some(function(number){

// 		return (number < 3);

// })
// console.log('Result form every()');
// console.log(someValid);

// // filter - returns a new array that meets the given condition

// let filterValid = numbers.filter(function(number){


// 		return (number < 3);

// })
// console.log('Result form filter()');
// console.log(filterValid);

// //includes 
// let products = ['Mouse', 'Keyborad', 'Laptop', 'Monitor'];


// let filterProducts = products.filter(function(product){


// 		return product.toLowerCase().includes('a');

// })
// console.log('Result form filter() & includes');
// console.log(filterProducts);


// //reduce
// /*let/const resulArray = arrayName.reduce(function(accumulator, currentValue){
// statement;
// })*/


// //Multi-dimensional Array

// let chessBoard = [
//     ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
//     ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
//     ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
//     ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
//     ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
//     ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
//     ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
//     ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
// ];

// console.log(chessBoard);
// console.log(chessBoard[1][4]);
// console.log('Pawn moves to f2 ' + chessBoard[1][5]);
// console.log('Knight moves to c7 ' + chessBoard[6][2]);


let fruits = ["banana", "grape", "orange", "strawberry"];
let match = fruits.filter(function(fruit) { return true; });
console.log(match.length);